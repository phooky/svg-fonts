# SVG Fonts

SVG fonts suitable for use with [Hershey Text v3](https://gitlab.com/oskay/hershey-text) and later

A curated subset of the fonts in this repository are included with the Hershey Text distribution.

----

#### Contributing

Contributions to this collection are welcome. Contributions may be made through an [issue](https://gitlab.com/oskay/svg-fonts/issues) or properly formatted merge request to this repository.
You can also contact us through the Evil Mad Scientist [contact form](https://shop.evilmadscientist.com/contact).

Please keep in mind the following guidelines:
*  Fonts should be in the [SVG 1.1 font format](https://www.w3.org/TR/SVG11/fonts.html) and tested to work well with Hershey Text.
*  Matched pairs consisting of an SVG font plus a matching TTF or OTF font are welcome. 
Font pairs like this should be tested to work well for automatic substitution in Hershey Text.
*  Fonts need not be stroke-based. Compatible SVG outline fonts, color fonts, and other novel styles are welcome.
*  Fonts should be licensed in such a way that they are compatible with open source distribution.
Where possible, we recommend to use the [SIL Open Font LIcense](https://scripts.sil.org/OFL) or a public
domain declaration. In cases of license ambiguity or more restrictive licensing terms, please ask.
*  Font credit and license information should ideally be embedded within the font file metadata.
*  Font credit and license information may also be included in separate readme file.
*  This is a curated collection. Potential contributions may be reviewed for quality, attribution, usefulness, and other factors.


----

#### Font license information

Individual credits and license information are embedded within the SVG fonts in this repository.

The classic Hershey fonts included are derived from
work by Dr. A. V. Hershey.

Additional modern "EMS" fonts in this distribution are
derivatives created from fonts licensed under the [SIL Open Font LIcense](https://scripts.sil.org/OFL). Credit and links for these fonts are given below.
Please see the individual font files for additional information.


<table>
<tbody>
<tr>

<th align="center" style="background:#f0f0f0;"><b>SVG font name</b></th>

<th align="center" style="background:#f0f0f0;"><b>A derivative of</b></th>

<th align="center" style="background:#f0f0f0;"><b>Designer</b></th>

<th align="center" style="background:#f0f0f0;"><b>Reference</b></th>

</tr>

<tr>

<td>EMS Allure</td>

<td>Allura</td>

<td>[Rob Leuschke, TypeSETit](http://www.typesetit.com)</td>

<td>[Google Font Page](https://fonts.google.com/specimen/Allura)</td>

</tr>

<tr>

<td>EMS Bird</td>

<td>Bilbo</td>

<td>[Rob Leuschke, TypeSETit](http://www.typesetit.com)</td>

<td>[Google Font Page](https://fonts.google.com/specimen/Bilbo)</td>

</tr>

<tr>

<td>EMS Bird Swash Caps</td>

<td>Bilbo Swash Caps</td>

<td>[Rob Leuschke, TypeSETit](http://www.typesetit.com)</td>

<td>[Google Font Page](https://fonts.google.com/specimen/Bilbo+Swash+Caps)</td>

</tr>

<tr>

<td>EMS Brush</td>

<td>Alex Brush</td>

<td>[Rob Leuschke, TypeSETit](http://www.typesetit.com)</td>

<td>[Google Font Page](https://fonts.google.com/specimen/Alex+Brush)</td>

</tr>

<tr>

<td>EMS Capitol</td>

<td>Sacramento</td>

<td>[Brian J. Bonislawsky, Astigmatic One Eye Typographic Institute](http://www.astigmatic.com)</td>

<td>[Google Font Page](https://fonts.google.com/specimen/Sacramento)</td>

</tr>

<tr>

<td>EMS Casual Hand</td>

<td>Covered By Your Grace</td>

<td>[Kimberly Geswein, Kimberly Geswein Fonts](http://www.kimberlygeswein.com/)</td>

<td>[Google Font Page](https://fonts.google.com/specimen/Covered+By+Your+Grace)</td>

</tr>

<tr>

<td>EMS Decorous Script</td>

<td>Petit Formal Script</td>

<td>[Impallari Type](http://www.impallari.com)</td>

<td>[Google Font Page](https://fonts.google.com/specimen/Petit+Formal+Script)</td>

</tr>

<tr>

<td>EMS Delight</td>

<td>Delius</td>

<td>Natalia Raices</td>

<td>[Google Font Page](https://fonts.google.com/specimen/Delius)</td>

</tr>

<tr>

<td>EMS Delight Swash Caps</td>

<td>Delius Swash Caps</td>

<td>Natalia Raices</td>

<td>[Google Font Page](https://fonts.google.com/specimen/Delius+Swash+Caps)</td>

</tr>

<tr>

<td>EMS Elfin</td>

<td>Mountains of Christmas</td>

<td>[Crystal Kluge, Tart Workshop](http://www.tartworkshop.com)</td>

<td>[Google Font Page](https://fonts.google.com/specimen/Mountains+of+Christmas)</td>

</tr>

<tr>

<td>EMS Felix</td>

<td>Felipa</td>

<td>[Fontstage](https://twitter.com/fontstage)</td>

<td>[Google Font Page](https://fonts.google.com/specimen/Felipa)</td>

</tr>

<tr>

<td>EMS Herculean</td>

<td>Poiret One</td>

<td>[Denis Masharov](https://www.myfonts.com/foundry/Denis_Masharov/)</td>

<td>[Google Font Page](https://fonts.google.com/specimen/Poiret+One)</td>

</tr>

<tr>

<td>EMS Invite</td>

<td>Tangerine</td>

<td>[Toshi Omagari](http://tosche.net/about)</td>

<td>[Google Font Page](https://fonts.google.com/specimen/Tangerine)</td>

</tr>

<tr>

<td>EMS League</td>

<td>League Script</td>

<td>[Haley Fiege, the League of Moveable Type](https://www.theleagueofmoveabletype.com)</td>

<td>[Google Font Page](https://fonts.google.com/specimen/League+Script)</td>

</tr>

<tr>

<td>EMS Little Princess</td>

<td>Princess Sofia</td>

<td>[Crystal Kluge, Tart Workshop](http://www.tartworkshop.com)</td>

<td>[Google Font Page](https://fonts.google.com/specimen/Allura)</td>

</tr>

<tr>

<td>EMS Misty Night</td>

<td>Foglihten No03</td>

<td>[Grzegorz L, GLUK fonts](http://www.glukfonts.pl)</td>

<td>[FontSquirrel Page](https://www.fontsquirrel.com/fonts/foglihten)</td>

</tr>

<tr>

<td>EMS Neato</td>

<td>Bad Script</td>

<td>[Roman Shchyukin, Gaslight](https://www.myfonts.com/foundry/Gaslight/)</td>

<td>[Google Font Page](https://fonts.google.com/specimen/Bad+Script)</td>

</tr>

<tr>

<td>EMS Osmotron</td>

<td>Orbitron (Regular)</td>

<td>[Matt McInerney, the League of Moveable Type](https://www.theleagueofmoveabletype.com)</td>

<td>[Google Font Page](https://fonts.google.com/specimen/Orbitron)</td>

</tr>

<tr>

<td>EMS Pancakes</td>

<td>Short Stack</td>

<td>[James Grieshaber, Typeco](http://www.typeco.com)</td>

<td>[Google Font Page](https://fonts.google.com/specimen/Short+Stack)</td>

</tr>

<tr>

<td>EMS Pepita</td>

<td>Pecita</td>

<td>[Philippe Cochy](http://pecita.eu/police-en.php)</td>

<td>[FontSquirrel Page](https://www.fontsquirrel.com/fonts/Pecita)</td>

</tr>

<tr>

<td>EMS Qwandry</td>

<td>Qwigley</td>

<td>[Rob Leuschke, TypeSETit](http://www.typesetit.com)</td>

<td>[Google Font Page](https://fonts.google.com/specimen/Qwigley)</td>

</tr>

<tr>

<td>EMS Readability</td>

<td>Source Sans Pro-Light</td>

<td>[Paul D. Hunt, Adobe](http://www.adobe.com)</td>

<td>[Google Font Page](https://fonts.google.com/specimen/Source+Sans+Pro)</td>

</tr>

<tr>

<td>EMS Readability Italic</td>

<td>Source Sans Pro-Light</td>

<td>[Paul D. Hunt, Adobe](http://www.adobe.com)</td>

<td>[Google Font Page](https://fonts.google.com/specimen/Source+Sans+Pro)</td>

</tr>

<tr>

<td>EMS Society</td>

<td>Mrs Saint Delafield</td>

<td>[Alejandro Paul, Sudtipos](http://www.sudtipos.com)</td>

<td>[Google Font Page](https://fonts.google.com/specimen/Mrs+Saint+Delafield)</td>

</tr>

<tr>

<td>EMS Swiss</td>

<td>Italianno</td>

<td>[Rob Leuschke, TypeSETit](http://www.typesetit.com)</td>

<td>[Google Font Page](https://fonts.google.com/specimen/Italianno)</td>

</tr>

<tr>

<td>EMS Tech</td>

<td>Architects Daughter</td>

<td>[Kimberly Geswein, Kimberly Geswein Fonts](http://www.kimberlygeswein.com/)</td>

<td>[Google Font Page](https://fonts.google.com/specimen/Architects+Daughter)</td>

</tr>

</tbody>

</table>



